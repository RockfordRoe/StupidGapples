package rockfordroe.stupidgapples;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public final class StupidGapples extends JavaPlugin {

    @Override
    public void onEnable() {
        ItemStack gapple = new ItemStack(Material.GOLDEN_APPLE, 1, (short)1);
        ShapedRecipe recipe = new ShapedRecipe(gapple);
        recipe.shape("###", "#@#", "###");
        recipe.setIngredient('#', Material.GOLD_BLOCK);
        recipe.setIngredient('@', Material.APPLE);
        getServer().addRecipe(recipe);
    }

}
